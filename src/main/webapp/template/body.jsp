<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10">
	<div class="d-flex">
		<div class="mr-auto p-2 align-self-center"
			style="text-transform: uppercase;">
			<h5>DANH MỤC THUỐC</h5>
		</div>
		<div class="p-2">
			<button type="button" class="btn btn-sm btn-success"
				data-toggle="modal" data-target="#nhapthuoc" data-backdrop="static">
				<i class="fas fa-plus-circle"></i> <label class="form-check-label"
					for="defaultCheck1">Nhập thuốc</label>
			</button>
		</div>
	</div>
	<div class="row" style="padding-top: 10px">
		<div class="col-12">
			<div class="table-responsive-lg table-1">
				<table id="tb-danhsachthuoc"
					class="table table-sm table-hover table-bordered">
					<!-- <table class="table table-sm table-hover"> -->
					<thead>
						<tr class="head-tb">
							<th scope="col" width="10%">Mã thuốc</th>
							<th scope="col" width="15">Tên thuốc</th>
							<th scope="col" width="20%">Nhóm thuốc</th>
							<th scope="col" width="10%">Giá bán</th>
							<th scope="col" width="10%">Giá gốc</th>
							<th scope="col" width="10%">Trạng thái</th>
							<th scope="col" width="10%">Tồn kho</th>
							<th scope="col" width="15%">Hành động</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0101</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-success">Còn hàng</span></td>
							<td>15</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
						<tr>
							<td>QQ0102</td>
							<td>Thuốc ho bổ phế</td>
							<td>Trị ho</td>
							<td>18000</td>
							<td>15000</td>
							<td><span class="badge badge-secondary">Hết hàng</span></td>
							<td>18</td>
							<td>
								<div class="btn-group mr-2" role="group"
									aria-label="First group">
									<button type="button" class="btn btn-info btn-sm">
										<i class="fas fa-pen-square"></i> <label
											class="form-check-label" for="defaultCheck1">Sửa</label>
									</button>
									<button type="button" class="btn btn-danger btn-sm">
										<i class="fas fa-trash-alt"></i> <label
											class="form-check-label" for="defaultCheck1">Xóa</label>
									</button>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade modal-nhapthuoc" id="nhapthuoc" role="dialog">
	<div class="modal-dialog modal-dialog-centered modal-lg"
		role="document" data-backdrop="static">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle">Nhập thuốc</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-8">
							<form>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-12 col-form-label col-form-label-sm label-modal">Mã
										thuốc</label>
									<div class="col-sm-10 col-12 ">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm" placeholder="Nhập mã thuốc">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-form-label col-form-label-sm label-modal">Tên
										thuốc</label>
									<div class="col-sm-10">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm" placeholder="Nhập tên thuốc">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-form-label col-form-label-sm label-modal">Nhóm
										thuốc</label>
									<div class="col-sm-10">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm"
											placeholder="Nhập tên nhóm thuốc: dùng ajax success các nhóm thuốc">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-form-label col-form-label-sm label-modal">Giá
										gốc</label>
									<div class="col-sm-5">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-form-label col-form-label-sm label-modal">Giá
										bán</label>
									<div class="col-sm-5">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-form-label col-form-label-sm label-modal">Tồn
										kho</label>
									<div class="col-sm-5">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm">
									</div>
								</div>
								<div class="form-group row">
									<label for="colFormLabelSm"
										class="col-sm-2 col-md-2 col-form-label col-form-label-sm label-modal">Đơn
										vị</label>
									<div class="col-sm-5 col-md-5">
										<select class="custom-select custom-select-sm">
											<option selected>Lựa chọn đơn vị thuốc: lọ, viên...</option>
											<option value="1">Viên</option>
											<option value="2">Lọ</option>
											<option value="3">Gói</option>
											<option value="3">Đơn vị khác</option>
										</select>
									</div>
									<label for="colFormLabelSm"
										class="col-sm-2 col-md-2 col-form-label col-form-label-sm label-modal">Số
										lượng</label>
									<div class="col-sm-3 col-md-3">
										<input type="text" class="form-control form-control-sm"
											id="colFormLabelSm">
									</div>
								</div>
							</form>
						</div>
						<div class="col-12 col-sm-12 col-md-12 col-lg-4">
							<div class="card text-center">

								<div class="card-body">
									<img class="card-img-top"
										src="http://photos.s-cute.com/130901/sample/contents/576_yuna/576_yuna_k04/002.jpg"
										alt="Card image cap">
								</div>
								<div class="card-footer text-muted">
									<a href="#" class="btn btn-sm btn-primary">Thêm ảnh</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer mr-auto">
				<button type="button" class="btn btn-success btn-sm">
					<i class="fas fa-save"></i> <label class="form-check-label"
						for="defaultCheck1">Lưu và thêm mới</label>
				</button>
				<button type="button" class="btn btn-secondary btn-sm"
					data-dismiss="modal">
					<i class="fas fa-ban"></i> <label class="form-check-label"
						for="defaultCheck1">Bỏ qua</label>
				</button>
			</div>
		</div>
	</div>
</div>